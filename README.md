## Example of Microservices Architecture

![alt text](arhitecture.png)

The project is developed to project the overall knowledge of the Microservices architecture.
Currently, Microservices is one of the most popular architectural design patterns, which can provide to a project such advantages as:
easy scalability, resilience and agility. Most of those advantages is very difficult to achieve using conventional Monolithic architecture.
Conceptually it is not new and been around for a long time, implemented on an enterprise-wide level and known as SOA.
The comunication between loosely coupled, granular services can be either via AMQP, HTTP/HTTPS or TCP.

The project includes: 
 - React as front end, from where a user can interact with the application and perform CRUD operation.
 - NGINX as the Api gateway and server for the services.
 - The Database service provides connection with the database for the rest of the services. Alternatively
 each service could have direct access to the database. That would cause redundant database package in each of the 
 services. Moreover, each service could have a dedicated database hosted either on the same or separate servers.
 On the other hand, the heavier and more multifunctional microservices become, the more they drift towards SOA.
 - The Email service sends emails. Can have wide range of email templates to send to various clients.
 - Integration services can provide access to the web applications, social network platforms and anything else,
 which allows integration.

NOTE: Number of services can scale up and down if the necessary configuration adjustments have been set up. For instance,
cache <key, value> storage, such as redis - could be added to the project or the identity service, so users could have JWT
token and navigate across different services with it.
The name and purpose of the services can be different and depends on technical specifications and an application's system
design. The front end part is a matter of preferences and can be represented by any JavaScript framework alone, as 
well as, PHP framework replaced with vanilla PHP or even C++ - the architecture conceptually will not change.

The project includes Messaging Queue as one of its services.
RabbitMQ - https://www.rabbitmq.com/tutorials/tutorial-one-php.html
Though in the communication between services AMQP can be substituted by HTTP/HTTPS or TCP, for queuing and executing all sorts of tasks
it is one of the best available options.

## Run the project
Pull the project and run `composer install` for each of the services.

After successful installation of the components:

Check the configuration `.env` files and set up variables according to your local environment. 

Migrate the database schema and the project is ready to run.

To start the services in docker containers:
 - WebClientReact/web-client - `yarn install`, `yarn build` and `docker build --tag=web_client .`
 - DatabaseService - `docker build --tag=db_service .`
 - EmailService - `docker build --tag=email_service .`
 - IntegrationService - `docker build --tag=integration_service .`

After the images has been built successfully run `docker-compose up`, and open `localhost:4000`

Run migration for both `development` and `test` databases, so the dockerized postgres would have tables to store data. 
- `docker exec -it php-fpm bash -c "php /services/db_service/bin/console doctrine:migrations:migrate"`.

To populate tables with records, so the application could have some records: 
 - `docker exec -it php-fpm bash -c "php /services/db_service/bin/console doctrine:fixtures:load"`

To add more objects by using RabbitMQ jobs queue:
 - http://localhost:8086/create-books-job?books=6
 - http://localhost:8086/process-books-job
 - `docker exec -it php-fpm bash -c "php /services/db_service/bin/console messenger:consume"` and select jobs queue

Alternative way is to:
 - access database and add a record for testing purposes - `psql -h 127.0.0.1 -p 5433 -U postgres -d development` and execute `INSERT INTO BOOK (id, title,    author, published_date) VALUES (1, 'test', 'test', '2018-01-01');`

Open ReactApp in the browser `localhost:4000` and can interract with the project.

To make sure that RabbitMQ communication between services and the database works:
 - open in the browser `localhost:15672/`, enter login: user, pass: user and open Queues tab
 - open in the browser `localhost:8085` (email service index page)
 - open in the browser `localhost:8087` (integration service index page)
 - `docker exec -it php-fpm bash -c "php /services/db_service/bin/console messenger:consume email_service integration_service"`
 - refresh `localhost:4000` and new book entries should appear, with the corresponding services names, instead of titles.
