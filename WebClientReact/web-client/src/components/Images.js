import React from 'react'
import { getConfig } from '../components/config';
import Carousel from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';
import Button from 'react-bootstrap/Button';

class Images extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            images: [],
            files: [],
            isLoaded: false,
            filesReady: false,

            selectedIndex: 0,
            direction: 0
        }
    }

    selectFile = (e) => {
        e.preventDefault();

        if (e.target.files) {
            Array.from(e.target.files).map((file) => {
                const reader = new FileReader();
                reader.onload = (ev) => {
                    this.setState({
                        images: [...this.state.images, ev.target.result]
                    });
                };

                reader.readAsDataURL(file);

                return 0;
            })

            this.setState({
                isLoaded: true
            });
        }
    }

    listImages = () => {
        fetch(getConfig().dbService + 'list-images')
        .then(res => res.json())
        .then(
            (result) => {
                this.setState({
                    files: result,
                    filesReady: true
                });
            }
        ).catch((error) => {
            console.error(error);
        })
    }

    handleSubmit = (action, method, data = []) => {
        fetch(getConfig().dbService + action, {
            method: method,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
        .then(
            this.setState({
                files: this.state.images,
                images: [],
                filesReady: true
            }),

            this.listImages()
        )
        .catch((error) => {
            console.error(error);
        })
    }

    removeImage(index) {
        const img = [...this.state.images];
        img.splice(index, 1);

        this.setState({
            images: img
        })
    }

    deleteImage = (e, index) => {
        e.preventDefault();
        if (this.state.files.length === 0) {
            return;
        }

        this.handleSubmit('delete-image', 'DELETE', { 'name': this.state.files[index] });

        const file = [...this.state.files];
        file.splice(index, 1);

        this.setState({
            files: file
        })
    }

    uploadImages = (e) => {
        e.preventDefault();
        if (this.state.images.length === 0) {
            return;
        }

        this.handleSubmit('upload-image', 'POST', this.state.images);
    }

    handleSelect = (selectedIndex, e) => {
        this.setState({
            selectedIndex: selectedIndex,
            direction: e.direction
        })
    };

    render() {
        const { images, isLoaded, filesReady, files } = this.state;

        return (
            <div>
                <h4 style={{ textAlign: "center", marginTop: "0.5%" }}>Image upload</h4>

                <Carousel arrows>
                    {filesReady ? files.map((image, i) =>
                        <div className="carouselitem">
                            <Button type="button" name="delete" className="close" onClick={(e) => this.deleteImage(e, i)}>
                                <span aria-hidden="true">&times;</span>
                            </Button>
                            <img src={getConfig().imagePath + image} alt="carouselimage" className="carouselimage" />
                        </div>
                    ) :
                        <div className="carouselitem">
                            <img src={"http://tineye.com/images/widgets/mona.jpg"} alt="defaultimage" className="carouselimage" />
                        </div>
                    }
                </Carousel>
                <div id="note">
                    <p>Current menu tab is not created to impress with design, rather to show files upload functionality in action.</p>
                    <p>NOTE: with additional code base modifications, the uploaded images can be edited (crop/size/color) before being uploaded.</p>
                    <p>*Upload images with the following extentions 'jpg', 'jpeg', 'gif', 'png'</p>
                </div>
                <div id="upload-buttons">
                    <label type="button" id="upload-label" >
                        <input id="upload" type="file" onChange={this.selectFile} multiple />
                    <span class="btn btn-outline-primary">Select files</span>
                    </label>
                    <Button variant="outline-primary" name="upload" onClick={this.uploadImages}>Upload</Button>
                </div>
                <br></br>
                <div id="images">
                    <ul id="preview">
                        {isLoaded ? images.map((image, i) =>
                            <li key={i} className="carouselitem">
                                <Button className="close" onClick={(e) => this.removeImage(i)}>
                                    <span aria-hidden="true">&times;</span>
                                </Button>
                                <img src={image} className="carouselimage" alt="img" />
                            </li>
                        ) : null
                        }
                    </ul>
                </div>
            </div>
        );
    }
}

export default Images;