import { getConfig } from '../components/config';
import React from 'react';
import { Button, Col, Form, ListGroup } from 'react-bootstrap';
import Select from 'react-select';

class Tweets extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
         keyWords: '',
         language: '',
         items: [],
         isLoaded: false
      };
   }

   onChange = (e) => {
      this.setState({
        [e.target.name]: e.target.value 
      });
   }

   findTweets = (e) => {
      e.preventDefault();
      const { keyWords, language } = this.state;

      if (keyWords === '' || language === '') {
         alert('Search keywords and language can not be empty.');
         return;
      }

      fetch(`${getConfig().integrationService}twitter-find?keywords=${keyWords}&language=${language}`, {
         headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
         },
      })
      .then(res => res.json())
      .then(
         (result) => {
            if ((result.status === 400 && result.message) || (result.status === 404 && result.message)) {
               alert(result.message);
               return;
            }

            const parsedJson = JSON.parse(result);
            console.log(parsedJson);
            this.setState({
               isLoaded: true,
               items: parsedJson.statuses
            });
         },
        (error) => {
            this.setState({
               isLoaded: true,
               error
            });
         }
      )
   }

   selectChange = (selectedOption) => {
      this.setState({ language: selectedOption.value });
   }

   render(){
      const { items, keyWords } = this.state;
      const options = [
        { value: "fr", label: "French" },
        { value: "en", label: "English" },
        { value: "ar", label: "Arabic" },
        { value: "ja", label: "Japanese" },
        { value: "es", label: "Spanish" },
        { value: "de", label: "German" },
        { value: "it", label: "Italian" },
        { value: "id", label: "Indonesian" },
        { value: "pt", label: "Portuguese" },
        { value: "ko", label: "Korean" },
        { value: "tr", label: "Turkish" },
        { value: "ru", label: "Russian" },
        { value: "nl", label: "Dutch"},
        { value: "fil", label: "Filipino"},
        { value: "msa", label: "Malay" },
        { value: "zh-tw", label: "Traditional Chinese" },
        { value: "zh-cn", label: "Simplified Chinese" },
        { value: "hi", label: "Hindi" },
        { value: "no", label: "Norwegian" },
        { value: "sv", label: "Swedish" },
        { value: "fi", label: "Finnish" },
        { value: "da", label: "Danish" },
        { value: "pl", label: "Polish" },
        { value: "hu", label: "Hungarian" },
        { value: "fa", label: "Farsi" },
        { value: "he", label: "Hebrew"},
        { value: "ur", label: "Urdu" },
        { value: "th", label: "Thai" },
        { value: "en-gb", label: "English UK"}
      ];
       
      return (
         <div>
           <h4 style={{textAlign: "center", marginTop:"0.5%", marginBottom:"2%"}}>Tweets</h4>
           <div>
            <Form horizontal="true" className="tweet-search">
               <Form.Row> 
                 <Form.Group as={Col}md="4">            
                   <Form.Control type="text" name="keyWords" value={keyWords} onChange={this.onChange} placeholder="Search..." />
                 </Form.Group>
                 <Form.Group as={Col} md="2"> 
                 <Select className="select-search"
                   onChange={this.selectChange}
                   options={options}
                   autoFocus={true}
                 />
                 </Form.Group>
                 <Form.Group as={Col}>
                   <Button name="search" onClick={this.findTweets}>Find</Button>
                 </Form.Group>  
               </Form.Row>
            </Form>
           </div>
           <br></br>
           <div id="tweets">
               {this.state.isLoaded ? items.map((item, index) => (
                <ListGroup key={index} >
                  <ListGroup.Item action active value={item.id}>
                    <p>id: {item.id}</p> 
                  </ListGroup.Item>
                    <ListGroup.Item>
                      <p>{item.created_at}</p>
                      <p>{item.text}</p>
                      <p>{item.user.name}</p>
                      <p>{item.user.location}</p>
                    </ListGroup.Item>
                  </ListGroup>  
                )) : null}
           </div>
         </div>
      )
   };
}
 
export default Tweets;
