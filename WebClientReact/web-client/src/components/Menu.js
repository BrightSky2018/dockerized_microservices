import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';

const Menu = () => {
   return (
      <div>
         <Navbar bg="dark" variant="dark">
            <Navbar.Brand href="/">
               <img
                  alt=""
                  src="/logo3.png"
                  width="40"
                  height="40"
                  className="d-inline-block align-center"
               />{' '}
            Simple App Example
         </Navbar.Brand>
            <Nav className="justify-content-end" style={{ width: "70%" }}>
               <Nav.Link href="/">Books</Nav.Link>
               <Nav.Link href="/tweets">Tweets</Nav.Link>
               <Nav.Link href="/images">Images</Nav.Link>
               <Nav.Link href="/email">Email</Nav.Link>
               <Nav.Link href="/about">About</Nav.Link>
            </Nav>
         </Navbar>
      </div>
   );
}

export default Menu;
