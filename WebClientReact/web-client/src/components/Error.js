import React from 'react';

const Error = () => {
   return (
      <div>
         <p>Error: There is no such page!</p>
      </div>
   );
}

export default Error;
