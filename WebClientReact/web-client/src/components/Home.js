import { getConfig } from './config';
import React from 'react';
import Button from 'react-bootstrap/Button';
import { ListGroup, Col, Form } from 'react-bootstrap';
import ReactPagination from "react-js-pagination";
import Select from 'react-select';
import BookForm from './BookForm';
import ModalForm from './ModalForm';

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      //main vars
      items: [],
      filteredData: [],
      title: '',
      author: '',

      //dates
      publishedDate: '',
      startDate: '',
      endDate: '',

      //input
      field: '',
      keyWords: '',
      searchSelect: '',

      //modal forms and pagination
      isLoaded: false,
      showBooksForm: false,
      showModal: false,
      checked: false,
      activePage: 1,
      booksPerPage: 10
    };
  }

  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  toggleBookForm = (_id = 0, _title = '', _author = '') => {
    this.setState({
      showBooksForm: !this.state.showBooksForm,
      id: _id,
      title: _title,
      author: _author,
      selectedOption: null,
    });
  }

  toggleModalForm = (event) => {
    this.setState({
      id: event ? event.target.value : null,
      showModal: !this.state.showModal
    });
  }

  getRequest() {
    const { keyWords, searchSelect, startDate, endDate } = this.state;
    let action = 'list-books';

    if (keyWords && !searchSelect) {
      alert('Select field to search by. The field can not be empty.');
    }

    if ((keyWords && searchSelect) || (startDate && endDate)) {
      const params = '?keywords=' + keyWords + '&start-date=' + startDate + '&end-date=' + endDate + '&field=' + searchSelect;
      action = action + params;
    }

    fetch(getConfig().dbService + action)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            items: result,
            filteredData: result
          });
        }).catch((error) => {
          console.error(error);
        }
      );
  }

  componentDidMount() {
    this.getRequest()
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { title, author, publishedDate } = this.state;

    let action = 'add-book';
    let method = 'POST';

    if (e.target.name === 'search') {
      this.getRequest();
      return;
    }

    if (e.target.name === 'edit') {
      action = 'update-book';
      method = 'PATCH';
      this.setState({ showBooksForm: false })
    }

    if (e.target.name === 'delete') {
      action = 'delete-book';
      method = 'DELETE';
      this.setState({ showModal: false })
    }

    fetch(getConfig().dbService + action, {
      method: method,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        'id': e.target.value,
        'title': title,
        'author': author,
        'publishedDate': publishedDate
      })
    }).then(
      (result) => {
        this.componentDidMount();
      }).catch((error) => {
        console.error(error);
      });
  };

  handleSort = (e) => {
    const { checked, field, items } = this.state;

    if (!field) {
      alert('Select field to sort by. The field can not be empty.');
    }

    checked ? items.sort((a, b) => a[field] > b[field])
      : items.sort((a, b) => a[field] < b[field])
    this.setState({ items, checked: !checked })
  }

  handlePageChange(pageNumber) {
    // here to put code. on page change increase counter 
    // of active page and trig request for new records if
    // the number of pages close to run out
    this.setState({ activePage: pageNumber, offset: pageNumber * this.state.booksPerPage });
  }

  handleFilter = (e) => {
    const { filteredData } = this.state;

    let keyWord = null;
    e.target.value ? keyWord = e.target.value.toLowerCase() : this.componentDidMount()

    let data = filteredData.filter(item => {
      return Object.keys(item).some(key =>
        typeof item[key] === "string" && item[key].toLowerCase().includes(keyWord)
      )
    });

    this.setState({ items: data, activePage: 1 });
  };

  selectChange = (selectedOption) => {
    this.setState({ field: selectedOption.value });
  }

  searchSelectChange = (selectedOption) => {
    this.setState({ searchSelect: selectedOption.value });
  }

  render() {
    const { items, filter, showModal, showBooksForm, id, title, author, keyWords } = this.state;
    const options = [
      { value: '', label: 'Select...' },
      { value: 'id', label: 'id' },
      { value: 'title', label: 'title' },
      { value: 'author', label: 'author' },
      { value: 'publishedDate', label: 'published date' },
    ];

    // Logic for displaying current records
    const indexOfLastTodo = this.state.activePage * this.state.booksPerPage;
    const indexOfFirstTodo = indexOfLastTodo - this.state.booksPerPage;
    const currentTodos = items.length > 0 ? items.slice(indexOfFirstTodo, indexOfLastTodo) : null;

    return (
      <div>
        <h4 style={{ textAlign: "center", marginTop: "0.5%" }}>Books</h4>

        <ModalForm
          show={showModal}
          onHide={this.toggleModalForm}
          id={id}
          handleSubmit={this.handleSubmit}
        />

        <BookForm
          show={showBooksForm}
          onHide={this.toggleBookForm}
          id={id}
          title={title}
          author={author}
          handleChange={this.onChange}
          handleSubmit={this.handleSubmit}
        />

        <Form horizontal="true" className="form-search">
          <Form.Row>
            <Form.Group as={Col} md="4">
              <Select className="select-search"
                onChange={this.searchSelectChange}
                options={options}
                autoFocus={true}
              />
            </Form.Group>

            <Form.Group as={Col} md="6">
              <Form.Control type="text" name="keyWords" value={keyWords} onChange={this.onChange} placeholder="Search..." />
            </Form.Group>
            <Form.Group as={Col}>
              <Button name="search" onClick={this.handleSubmit}>Find</Button>
            </Form.Group>
          </Form.Row>

          <Form.Row>
            <Form.Group as={Col} md="4">
              <Form.Control type="date" name="startDate" onChange={this.onChange} />
            </Form.Group>
            <Form.Group as={Col} md="4">
              <Form.Control type="date" name="endDate" onChange={this.onChange} />
            </Form.Group>
          </Form.Row>
        </Form>

        <Form horizontal="true" className="filter-form">
          <Form.Row>
            <Form.Group as={Col} sm="1.2">
              <Button variant="outline-primary" name="add" onClick={this.toggleBookForm.bind(this)}>Add</Button>
            </Form.Group>

            <Form.Group as={Col} md="5">
              <Form.Control className="filter"
                placeholder="Filter..."
                value={filter}
                onChange={this.handleFilter}
              />
            </Form.Group>

            <Form.Group as={Col} md="4">
              <Select className="select-filter"
                onChange={this.selectChange}
                options={options}
                autoFocus={true}
              />
            </Form.Group>

            <Form.Group as={Col}>
              <Button variant="outline-info" onClick={this.handleSort}>Sort</Button>
            </Form.Group>
          </Form.Row>
        </Form>
        <div id="scroll">
          {this.state.isLoaded ? currentTodos.map((item, index) => (
            <ListGroup key={index} >
              <ListGroup.Item action active value={item.id}
                onClick={this.toggleBookForm.bind(this, item.id, item.title, item.author, item.publishedDate)}>
                {item.id}
              </ListGroup.Item>
              <ListGroup.Item>
                <p>{item.title} {item.publishedDate}</p>
                <p>{item.author}</p>

                <Button variant="outline-danger" className="float-sm-right"
                  value={item.id} onClick={this.toggleModalForm.bind(this)} name='delete'>Delete</Button>
              </ListGroup.Item>
            </ListGroup>
          )) : null}
        </div>

        <ReactPagination
          itemClass="page-item"
          linkClass="page-link"
          hideNavigation
          activePage={this.state.activePage}
          itemsCountPerPage={this.state.booksPerPage}
          totalItemsCount={this.state.items.length}
          onChange={this.handlePageChange.bind(this)}
        />

      </div>
    );
  }
}

export default Home;
