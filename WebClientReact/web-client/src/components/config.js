const devAPIs = {
    dbService: 'http://localhost:8086/',
    integrationService: 'http://localhost:8087/',
    emailService: 'http://localhost:8085/',
    imagePath: 'http://localhost:8086/images/'
};

export function getConfig() {
    return devAPIs;
}