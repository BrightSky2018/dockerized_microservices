import React from 'react';
import Button from 'react-bootstrap/Button';
import { Modal } from 'react-bootstrap';

class ModalForm extends React.Component {

  render() {
    return (
      <Modal show={this.props.show} onHide={this.props.onHide}>
        <Modal.Header closeButton>
          <Modal.Title></Modal.Title>
        </Modal.Header>
        <Modal.Body>Do you really want to delete this book !</Modal.Body>
        <Modal.Footer>
          <Button variant="outline-danger" onClick={this.props.onHide}>
            Cancel
          </Button>
          <Button variant="outline-primary" name="delete" value={this.props.id} onClick={this.props.handleSubmit}>
            Delete
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
};

export default ModalForm;