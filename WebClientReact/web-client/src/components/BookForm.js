import React from 'react';
import Button from 'react-bootstrap/Button';
import { Col, Form, Modal } from 'react-bootstrap';

class BookForm extends React.Component {

  render() {
    let name, value = '';
    name = 'Add'

    if (this.props.id > 0) {
      name = 'Edit'
      value = this.props.id
    }

    return (
      <Modal show={this.props.show} onHide={this.props.onHide} aria-labelledby="contained-modal-title-vcenter">
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            {name} Book
        </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form horizontal="true" className="book-form" aria-labelledby="contained-modal-title-vcenter">
            <Form.Row>
              <Form.Group as={Col} md="4">
                <Form.Label>Title</Form.Label>
                <Form.Control
                  required
                  type="text"
                  placeholder="Book title"
                  name='title'
                  value={this.props.title}
                  onChange={this.props.handleChange}
                />
              </Form.Group>
              <Form.Group as={Col} md="4">
                <Form.Label>Author</Form.Label>
                <Form.Control
                  required
                  type="text"
                  placeholder="Book author"
                  name='author'
                  value={this.props.author}
                  onChange={this.props.handleChange}
                />
              </Form.Group>
              <Form.Group as={Col} md="4">
                <Form.Label>Date</Form.Label>
                <Form.Control
                  required
                  type="date"
                  name='publishedDate'
                  value={this.props.publishedDate}
                  onChange={this.props.handleChange}
                />
              </Form.Group>
            </Form.Row>
            <Form.Row>
              <Form.Group as={Col} md="2">
                <Button variant="outline-primary" name={name.toLowerCase()} value={value} onClick={this.props.handleSubmit}>{name}</Button>
              </Form.Group>
            </Form.Row>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="outline-danger" onClick={this.props.onHide}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default BookForm;
