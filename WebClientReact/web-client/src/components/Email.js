import React from 'react';
import { getConfig } from './config';
import { Button, Col, Form, ListGroup } from 'react-bootstrap';

class Email extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
         email: '',
         topic: '',
         text: '',
      };
   }

   handleSubmit = (e) => {
      e.preventDefault();
      const { email, topic, text } = this.state;

      const mail = {
         'from': email,
         'to': email,
         'subject': topic,
         'body': text
      };

      fetch(getConfig().emailService + 'send-email', {
         method: 'POST',
         headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
         },
         body: JSON.stringify(mail)
      }).then(
         (result) => {
            console.log(result)
         },
         (error) => {
            console.log(error)
         })
   };

   onChange = (event) => {
      this.setState({
         [event.target.name]: event.target.value
      });
   }

   render() {
      const { email, topic, text } = this.state;

      return (
          <div>
          <h4 style={{textAlign: "center", marginTop:"0.5%", marginBottom:"2%"}}>Tweets</h4>
          <div>
           <Form horizontal="true" className="mail-form">
              <Form.Row> 
                <Form.Group as={Col}md="3">            
                  <Form.Control type="text" name='email' value={email} onChange={this.onChange} placeholder="email address..." />
                </Form.Group>
                <Form.Group as={Col} md="3"> 
                  <Form.Control type="text" name='topic' value={topic} onChange={this.onChange} placeholder="topic..." />
                </Form.Group>
                <Form.Group as={Col} md="4"> 
                  <Form.Control type="text" name='text' value={text} onChange={this.onChange} placeholder="message..." />
                </Form.Group>
                <Form.Group as={Col}>
                  <Button name="submit" value='send-email' onClick={this.handleSubmit}>Send</Button>
                </Form.Group>  
              </Form.Row>
           </Form>
          </div>
        </div>
      )
   };
}

export default Email;
