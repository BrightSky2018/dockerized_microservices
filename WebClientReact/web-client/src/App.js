import React from 'react';
import './App.css';
import { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

// import components
import Home from './components/Home';
import Tweets from './components/Tweets';
import Images from './components/Images';
import Email from './components/Email';
import Menu from './components/Menu';
import Error from './components/Error';
// import * as ReactBootstrap from 'react-bootstrap';

class App extends Component {

  render() {
    return (
      <BrowserRouter>
        <div>
          <Menu />
          <Switch>
            <Route path="/" component={Home} exact />
            <Route path="/tweets" component={Tweets} exact />
            <Route path="/images" component={Images} exact />
            <Route path="/email" component={Email} exact />
            <Route component={Error} />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
