<?php

namespace App\MessageHandler;

use App\Message\IntegrationService;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class IntegrationServiceMessageHandler implements MessageHandlerInterface
{
    public function __invoke(IntegrationService $message)
    {
    }
}