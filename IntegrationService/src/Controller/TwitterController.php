<?php
/*
* It is just an example of 3rd API integration. Can integrate anything,
* whatever provides access to their endpoints.
*/

namespace App\Controller;

use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Psr\Log\LoggerInterface;
use GuzzleHttp\Client;
use App\Crypto\Encryption;
use App\Message\IntegrationService;
use Abraham\TwitterOAuth\TwitterOAuth;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

final class TwitterController extends AbstractFOSRestController
{
    /**
     * @var TwitterOAuth
     */
    private $connection;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var mixed
     */
    private $response;

    public function __construct(LoggerInterface $logger)
    {
        $this->client = new Client();
        $this->connection = new TwitterOAuth(
            $_ENV['ApiKey'],
            $_ENV['ApiSecretKey'],
            $_ENV['AccessToken'],
            $_ENV['AccessTokenSecret']
        );
        $this->content = $this->connection->get("account/verify_credentials");
        $this->logger = $logger;
    }

    /**
     * @param MessageBusInterface $bus
     * @return View
     */
    public function index(MessageBusInterface $bus): View
    {
        /*
        * The message can be anything, even serialized objects
        * to send, so database service can store them. This is
        * just an example of service comunication using RabbitMQ.
        * If the data exchange is large RabbitMQ has to be replaced
        * by Kafka. For small transactions and services interactions
        * RabbitMQ is more than enought.
        */

        $message = new IntegrationService('Integration service');
        $bus->dispatch($message);

        return new View(
            'Twitter Controller !',
            Response::HTTP_OK
        );
    }

    /**
     * @param Request $request
     * @return View
     */
    public function findTweets(Request $request): View
    {
        try {
            $searchQuery = [
                "q" => '#' . implode(" #", explode(" ", $request->get('keywords'))),
                "count" => 200,
                "result_type" => "recent",
                "lang" => $request->get('language')
            ];

            $this->response = json_encode($this->connection->get('search/tweets', $searchQuery));
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
            throw new NotFoundHTTPException($e->getMessage());
        }

        // returns stringified json, which has to be parsed
        return new View(
            $this->response,
            Response::HTTP_OK
        );
    }

    /**
     * @param Request $request
     * @return View
     */
    public function updateStatus(Request $request): View
    {
        try {
            $this->connection->post("statuses/update", ["status" => "hello world"]);
            $this->response = 'Status has been updated.';
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
            throw new BadRequestHttpException($e->getMessage());
        }

        return new View(
            $this->response,
            Response::HTTP_OK
        );
    }

    /**
     * @param Request $request
     * @return View
     */
    public function saveTweets(Request $request): View
    {
        try {
            $this->response = 'Records has been saved.';
            // send request to the databsae serice to save tweets
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
            throw new BadRequestHttpException($e->getMessage());
        }

        return new View(
            $this->response,
            Response::HTTP_CREATED
        );
    }

    /**
     * @param Request $request
     * @return View
     */
    public function deleteSavedTweets(Request $request): View
    {
        try {
            $this->response = 'Records has been deleted.';
            // send request to the database service to delete tweets
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
            throw new BadRequestHttpException($e->getMessage());
        }

        return new View(
            $this->response,
            Response::HTTP_OK
        );
    }
}
