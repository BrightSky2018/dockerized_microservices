<?php
/*
* It is just an example of 3rd API integration. Can integrate anything,
* whatever provides access to their endpoints.
*/
namespace App\Controller;

use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

final class SoundcloudController extends AbstractFOSRestController
{
    // sound cloud logic goes here
}     
