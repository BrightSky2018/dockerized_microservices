<?php

namespace App\EventListener;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Psr\Log\LoggerInterface;

final class ExceptionListener
{
    private $urlGenerator;
    private $logger;

    public function __construct(UrlGeneratorInterface $urlGenerator, LoggerInterface $logger)
    {
        $this->urlGenerator = $urlGenerator;
        $this->logger = $logger;
    }
    
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();               
        $request = Request::createFromGlobals();

        if ($exception instanceof \Exception) {
            $this->logger->error($exception->getMessage(), $exception->getTrace());
            $action = $request->headers->get('referer');
        }

        if (!$action) {
            $action = $this->urlGenerator->generate('index');
        }

        // $event->setResponse(new RedirectResponse($action));
    }
}