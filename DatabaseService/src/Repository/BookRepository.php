<?php

namespace App\Repository;

use App\Entity\Book;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityNotFoundException;

/**
 * @method Book|null find($id, $lockMode = null, $lockVersion = null)
 * @method Book|null findOneBy(array $criteria, array $orderBy = null)
 * @method Book[]    findAll()
 * @method Book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class BookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Book::class);
    }

    // /**
    //  * @return Book[] Returns an array of Book objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Book
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * @throws EntityNotFoundException
     * @return array
     */
    public function _findAll(): ?array
    { 
        if (!$entities = $this->findAll()) {
            throw new EntityNotFoundException(
                'Could not find any records.'
            );
        }

        return $entities;
    }

    /**
     * @return Job[] Returns an array of Job objects
     * @throws EntityNotFoundException
     */
    public function findByWithBetweenDates(array $params = []): ?array
    {
        $qb = $this->createQueryBuilder('j');

        if (!empty($params['start-date']) && !empty($params['end-date'])) {
            $qb->where('j.published_date >= :begin')
               ->setParameter('begin', $params['start-date'])
               ->andWhere('j.published_date <= :end')
               ->setParameter('end', $params['end-date']);
        }
     
        if (!empty($params['keywords']) && !empty($params['field'])) {
            $qb->andWhere('j.'.$params['field'].' LIKE :'.$params['field'].'')
               ->setParameter($params['field'], '%'.$params['keywords'].'%');
        }

        if (!$qb->getQuery()->getResult()) {
            throw new EntityNotFoundException(
                'Could not find any records.'
            );
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Book $entity
     * @return void
     */
    public function update(Book $entity)
    {
        if (!$this->findOneById($entity->getId())) {
            throw new EntityNotFoundException(sprintf(
                'The resource with id "%s" was not found.',
                $entity->getId()
            ));
        }

        return $this->save($entity);
    }

    /**
     * @param Book $entity
     * @return void
     */
    public function delete(Book $entity): void
    {
        if (!$this->findOneById($entity->getId())) {
            throw new EntityNotFoundException(sprintf(
                'The resource with id "%s" does not exists.',
                $entity->getId()
            ));
        }
        
        $this->_em->remove($this->findOneById($entity->getId()));
        $this->_em->flush();
    }

    /**
     * @param Book $entity
     * @return void
     */
    public function save(Book $entity): void
    {
        if (!$entity->getId()) {
            $this->_em->persist($entity);
        } else {
            $this->_em->merge($entity);
        }

        $this->_em->flush();
    }
}
