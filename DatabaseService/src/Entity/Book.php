<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BookRepository")
 */
class Book
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank
     * @Assert\Regex(pattern="/^[A-Za-z]+$/")
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    private $title;

    /**
     * @Assert\NotBlank
     * @Assert\Regex(pattern="/^[A-Za-z]+$/")
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    private $author;

    /**
     * @ORM\Column(type="date")
     * @Assert\Date()
     * @Assert\NotBlank()
     */
    private $published_date;

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getAuthor(): string
    {
        return $this->author;
    }

    public function setAuthor(string $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function setPublishedDate(\DateTimeInterface $date): self
    {
        $this->published_date = $date;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getPublishedDate(): ?\DateTimeInterface
    {
        return $this->published_date;
    }

}
