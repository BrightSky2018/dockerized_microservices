<?php

namespace App\Helpers\Validation;

use App\Entity\Book;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\RecursiveValidator;
use App\Exception\ValidationFailedException;

final class EntityValidator
{
    /**
     * @param Book $entity
     * @throws ValidationFailedException
     */
    public function basicValidation(Book $entity): void
    {
        $validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping()
            ->getValidator();

        $errors = $validator->validate($entity);
        if (count($errors)) {
            $errorMessages = $this->getErrorMessages($errors);
            throw new ValidationFailedException(implode(', ', $errorMessages));
        }
    }

    /**
     * @param $errors
     * @return array
     */
    private function getErrorMessages($errors): array
    {
        $errorMessages = [];

        foreach ($errors as $error) {
            $errorMessages[] = sprintf(
                '%s: %s',
                $error->getPropertyPath(),
                $error->getMessage()
            );
        }

        return $errorMessages;
    }
}