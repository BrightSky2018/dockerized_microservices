<?php

namespace App\MessageHandler;

use App\Entity\Book;
use App\Message\EmailService;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Doctrine\ORM\EntityManagerInterface;

final class EmailServiceMessageHandler implements MessageHandlerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {    
        $this->entityManager = $entityManager;
    }

    /**
     * @param EmailService $message
     * @return void
     */
    public function __invoke(EmailService $message): void
    {
        /*
        * This is an example of recieving message from another
        * service. The message, as well as the folloing after it
        * logic can be any and depends on business needs.
        */
        if ($message) {            
            $book = new Book();
            $book->setTitle($message->getContent());
            $book->setAuthor('New Book');
            $book->setPublishedDate(new \DateTime());

            $this->entityManager->persist($book);
            $this->entityManager->flush();
        }
    }
}