<?php

namespace App\MessageHandler;

use App\Entity\Book;
use App\Message\CreateBookJob;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Doctrine\ORM\EntityManagerInterface;

final class JobMessageHandler implements MessageHandlerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {    
        $this->entityManager = $entityManager;
    }

    /**
     * @param CreateBookJob $message
     * @return void
     */
    public function __invoke(CreateBookJob $message): void
    {
        /*
        * This is an example of recieving message from another
        * service. The message, as well as the folloing after it
        * logic can be any and depends on business needs.
        */
        if ($message) {          
            $content = $message->getContent();
  
            $book = new Book();
            $book->setTitle($content['title']);
            $book->setAuthor($content['author']);
            $book->setPublishedDate($content['publishedDate']);

            $this->entityManager->persist($book);
            $this->entityManager->flush();
        }
    }
}