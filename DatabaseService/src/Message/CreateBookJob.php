<?php

namespace App\Message;

final class CreateBookJob
{
    /**
     * @var array $content
     */
    private $content;

    public function __construct(array $content)
    {
        $this->content = $content;
    }

    /**
     * @return array
     */
    public function getContent(): array
    {
        return $this->content;
    }
}