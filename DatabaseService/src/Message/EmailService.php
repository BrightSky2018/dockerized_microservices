<?php

namespace App\Message;

final class EmailService
{
    /**
     * @var string $content
     */
    private $content;

    public function __construct(string $content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }
}