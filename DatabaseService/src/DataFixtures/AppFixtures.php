<?php

namespace App\DataFixtures;

use App\Entity\Book;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $book = new Book();
        $book->setTitle('testbook');
        $book->setAuthor('testAuthor');
        $book->setPublishedDate(new \DateTime());
        $manager->persist($book);
        $manager->flush();
    }
}
