<?php
/*
* Database service can serve to multiple services for CRUD operations.
* All other services can request it either via REST or RabbitMQ.
* NOTE: The REST implementation can be done using native Symfony tools,
* however, for this particular example FOS REST bundle has been used.
*/
namespace App\Controller;

use App\Repository\BookRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Psr\Log\LoggerInterface;
use App\Helpers\Validation\EntityValidator;

final class ImageController extends AbstractFOSRestController
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var mixed
     */
    private $response;

    /**
     * @var Serializer
     */
    private $serializer;

    public function __construct(BookRepository $repository, EntityValidator $validator, LoggerInterface $logger)
    {     
        $this->repository = $repository;
        $this->validator = $validator;
        $this->logger = $logger;
        $this->serializer = new Serializer(
            [new DateTimeNormalizer(),
            new ObjectNormalizer(null, null, null, new ReflectionExtractor())],
            [new JsonEncoder()]
        );
    }

    /**
     * @return View
     */
    public function index(): View
    {        
        return new View(
            'Image Controller !', 
            Response::HTTP_OK
        );
    }

    /**
     * @param Request $request
     * @return View
     */
    public function uploadImage(Request $request): View
    {
        try {
            $images = json_decode($request->getContent(), true);
    
            for ($i = 0; $i < sizeof($images); $i++) {
                if (preg_match('/^data:image\/(\w+);base64,/', $images[$i], $type)) {
                    $image = substr($images[$i], strpos($images[$i], ',') + 1);
                    $type = strtolower($type[1]); // jpg, png, gif
                
                    if (!in_array($type, ['jpg', 'jpeg', 'gif', 'png'])) {
                        throw new \Exception('Invalid image type.');
                    }
                
                    $image = base64_decode($image);
                
                    if ($image === false) {
                        throw new \Exception('base64_decode failed.');
                    }
                } else {
                    throw new \Exception('did not match data URI with image data.');
                }

                file_put_contents($this->getParameter('kernel.project_dir') . "/public/images/img_{$i}.{$type}", $image);
            }
        } catch(\Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
            throw new BadRequestHttpException($e->getMessage());
        }

        return new View(
            'The image has been uploaded.',
            Response::HTTP_OK
        );
    }

    /**
     * @param Request $request
     * @return View
     */
    public function listImages(Request $request): View
    {   
        try {
            $this->response = [];
            
            if ($handle = opendir($this->getParameter('kernel.project_dir') . '/public/images/')) {
                while (($entry = readdir($handle)) !== false) {
                    if ($entry != "." && $entry != "..") {
                        array_push($this->response, $entry);
                    }
                }
    
                closedir($handle);
            }
        } catch(\Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
            throw new NotFoundHTTPException($e->getMessage());
        }

        return new View(
            $this->response,
            Response::HTTP_OK
        );
    }

    /**
     * @param Request $request
     * @return View
     */
    public function deleteImage(Request $request): View
    {   
        try {
            $file = json_decode($request->getContent(), true);
    
            if (file_exists($this->getParameter('kernel.project_dir') . "/public/images/{$file['name']}")) {
                unlink($this->getParameter('kernel.project_dir') . "/public/images/{$file['name']}");
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
            throw new NotFoundHTTPException($e->getMessage());
        }

        return new View(
            'Image has been deleted.',
            Response::HTTP_OK
        );
    }
}
