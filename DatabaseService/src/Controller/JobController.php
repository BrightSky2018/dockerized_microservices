<?php
/*
* Database service can serve to multiple services for CRUD operations.
* All other services can request it either via REST or RabbitMQ.
* NOTE: The REST implementation can be done using native Symfony tools,
* however, for this particular example FOS REST bundle has been used.
*/

namespace App\Controller;

use App\Repository\BookRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use App\Message\CreateBookJob;
use Symfony\Component\Messenger\MessageBusInterface;
use Psr\Log\LoggerInterface;
use App\Helpers\Validation\EntityValidator;

final class JobController extends AbstractFOSRestController
{
    /**
     * @var BookRepository
     */
    private $repository;

    /**
     * @var EntityValidaor
     */
    private $validator;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(BookRepository $repository, EntityValidator $validator, LoggerInterface $logger)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->logger = $logger;
        $this->serializer = new Serializer(
            [
                new DateTimeNormalizer(),
                new ObjectNormalizer(null, null, null, new ReflectionExtractor())
            ],
            [new JsonEncoder()]
        );
    }

    /**
     * @return View
     */
    public function index(): View
    {
        return new View(
            'Job Controller !',
            Response::HTTP_OK
        );
    }

    /**
     * @param Request $request
     * @param MessageBusInterface $bus
     * 
     * @return View
     */
    public function createBooksJob(Request $request, MessageBusInterface $bus): View
    {
        try {
            $randString = function () {
                $chars = "abcdefghijklmnopqrstuvwxyz";
                $string = '';

                for ($i = 0; $i < 6; $i++) {
                    $string .= substr($chars, rand(0, strlen($chars) - 1), 1);
                }

                return $string;
            };

            for ($i = 0; $i < $request->get('books'); $i++) {
                $bus->dispatch(new CreateBookJob([
                    'title' => $randString(),
                    'author' => $randString(),
                    'publishedDate' => new \DateTime()
                ]));
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
            throw new BadRequestHttpException($e->getMessage());
        }

        return new View(
            'Jobs has been successfully created.',
            Response::HTTP_OK
        );
    }

    /**
     * @return View
     */
    public function processBooksJob(): View
    {
        $output = shell_exec('php ../bin/console messenger:consume jobs');

        return new View(
            "<pre>$output</pre>",
            Response::HTTP_OK
        );
    }
}
