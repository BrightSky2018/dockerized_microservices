<?php
/*
* Database service can serve to multiple services for CRUD operations.
* All other services can request it either via REST or RabbitMQ.
* NOTE: The REST implementation can be done using native Symfony tools,
* however, for this particular example FOS REST bundle has been used.
*/

namespace App\Controller;

use App\Entity\Book;
use App\Repository\BookRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Psr\Log\LoggerInterface;
use App\Helpers\Validation\EntityValidator;

final class BookController extends AbstractFOSRestController
{
    /**
     * @var BookRepository
     */
    private $repository;

    /**
     * @var EntityValidaor
     */
    private $validator;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var mixed
     */
    private $response;

    /**
     * @var Serializer
     */
    private $serializer;

    public function __construct(BookRepository $repository, EntityValidator $validator, LoggerInterface $logger)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->logger = $logger;
        $this->serializer = new Serializer(
            [
                new DateTimeNormalizer(),
                new ObjectNormalizer(null, null, null, new ReflectionExtractor())
            ],
            [new JsonEncoder()]
        );
    }

    /**
     * @return View
     */
    public function index(): View
    {
        return new View(
            'Book Controller !',
            Response::HTTP_OK
        );
    }

    /**
     * @param Request $request
     * @return View
     */
    public function addBook(Request $request): View
    {
        try {
            $book = $this->serializer->deserialize(
                $request->getContent(),
                Book::class,
                'json',
                ['ignored_attributes' => ['id']]
            );

            $this->validator->basicValidation($book);
            $this->response = $this->repository->save($book);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
            throw new BadRequestHttpException($e->getMessage());
        }

        return new View(
            $this->response,
            Response::HTTP_CREATED
        );
    }

    /**
     * @param Request $request
     * @return View
     */
    public function updateBook(Request $request): View
    {
        try {
            $book = $this->serializer->deserialize($request->getContent(), Book::class, 'json');
            $this->validator->basicValidation($book);
            $this->response = $this->repository->update($book);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
            throw new BadRequestHttpException($e->getMessage());
        }

        return new View(
            $this->response,
            Response::HTTP_OK
        );
    }

    /**
     * @param Request $request
     * @return View
     */
    public function deleteBook(Request $request): View
    {
        try {
            $book = $this->serializer->deserialize(
                $request->getContent(),
                Book::class,
                'json',
                ['ignored_attributes' => ['publishedDate']]
            );
            $this->response = $this->repository->delete($book);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
            throw new BadRequestHttpException($e->getMessage());
        }

        return new View(
            $this->response,
            Response::HTTP_OK
        );
    }

    /**
     * @param Request $request
     * @return View
     */
    public function listBooks(Request $request): View
    {
        try {
            $this->response = $this->repository->findByWithBetweenDates($request->query->all());
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
            throw new NotFoundHTTPException($e->getMessage());
        }

        return new View(
            $this->response,
            Response::HTTP_OK
        );
    }
}
