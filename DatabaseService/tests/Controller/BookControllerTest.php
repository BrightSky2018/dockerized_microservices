<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/*
* The test could be done differently to test database workflow exclusivly,
* and would require to instantiate controller and to pass interfaces.
* Current integrational testing is developed to cover both API calls 
* using $client and the database fucntionality using DQL.
*
* Unit testing would make sense only in case of presense in the project
* functions with business logic, which could be tested independently.
*/
class BookControllerTest extends WebTestCase
{
    private $em;

    public function setUp(): void
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager()
        ;
    }

    public function testAddBooks()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/add-book',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            '{"title":"test","author":"test"}'
        );

        $this->assertEquals(Response::HTTP_CREATED, $client->getResponse()->getStatusCode());
    }

    public function testListBooks()
    {
        $client = static::createClient();
        $client->request('GET', '/list-books');

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function testUpdateBooks()
    {
        $query = $this->em->createQuery("SELECT u FROM App\Entity\Book u WHERE u.title = 'test'");
        $bookId = $query->getArrayResult();

        $client = static::createClient();
        $client->request(
            'PATCH',
            '/update-book',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            '{"id":"'.$bookId[0]['id'].'","title":"update","author":"update"}'
        );

        $query = $this->em->createQuery("SELECT u FROM App\Entity\Book u WHERE u.title = 'update'");
        $bookTitle = $query->getArrayResult();

        $this->assertEquals('update', $bookTitle[0]['title']);
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }


    public function testDeleteBooks()
    {
        $query = $this->em->createQuery("SELECT u FROM App\Entity\Book u WHERE u.title = 'update'");
        $bookId = $query->getArrayResult();
        
        $client = static::createClient();

        $client->request(
            'DELETE',
            '/delete-book',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            '{"id":"'.$bookId[0]['id'].'"}'
        );

        $query = $this->em->createQuery("SELECT u FROM App\Entity\Book u WHERE u.title = 'update'");
        $books = $query->getArrayResult();
        
        $this->assertEquals(0, sizeof($books));
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }
}
