<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Message\EmailService;
use Symfony\Component\Messenger\MessageBusInterface;
use App\Object\EmailMessage;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Psr\Log\LoggerInterface;

final class EmailController extends AbstractFOSRestController
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param MessageBusInterface $bus
     * @return View
     */
    public function index(MessageBusInterface $bus): View
    {
        /*
        * The message can be anything, even serialized objects
        * to send, so database service can store them. This is
        * just an example of service comunication using RabbitMQ.
        * If the data exchange is large RabbitMQ has to be replaced
        * by Kafka. For small transactions and services interactions
        * RabbitMQ is more than enought.
        */

        $message = new EmailService('Email service');
        $bus->dispatch($message);

        return new View(
            'Email Controller !',
            Response::HTTP_OK
        );
    }

    /**
     * @param Request $request
     * @return View
     */
    public function sendEmail(Request $request): View
    {
        try {
            $serializer = new Serializer([new ObjectNormalizer()], [new JsonEncoder()]);
            $email = $serializer->deserialize($request->getContent(), EmailMessage::class, 'json');

            $transport = (new \Swift_SmtpTransport($_ENV['SMTP'], $_ENV['PORT'], $_ENV['ENCRYPTION']))
                ->setUsername($_ENV['USERNAME'])
                ->setPassword($_ENV['PASSWORD']);

            $mailer = new \Swift_Mailer($transport);
            $message = (new \Swift_Message())
                ->setSubject($email->getSubject())
                ->setFrom($email->getFrom())
                ->setTo($email->getTo())
                ->setBody($email->getBody(), 'text/html');

            $mailer->send($message);

            $response = 'Email has been sent !';
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
            throw new BadRequestHttpException($e->getMessage());
        }

        return new View(
            $response,
            Response::HTTP_OK
        );
    }
}
