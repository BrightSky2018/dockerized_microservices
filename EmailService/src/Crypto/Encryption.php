<?php

/**
 *Encrytion class
 */

namespace App\Crypto;

final class Encryption
{
    /**
     * @param string $data
     * @return string
     */
    public static function encrypt(string $data): string
    {
        $encryption_key = base64_decode($_ENV['ENCRYPTION_KEY']);
        $ssl_bytes = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
        $encrypted = openssl_encrypt($data, 'aes-256-cbc', $encryption_key, 0, $ssl_bytes);

        return base64_encode($encrypted . '#' . $ssl_bytes);
    }

    /**
     * @param string $data
     * @return string
     */
    public static function decrypt(string $data): string
    {
        $encryption_key = base64_decode($_ENV['ENCRYPTION_KEY']);
        list($encrypted_data, $ssl_bytes) = explode('#', base64_decode($data), 2);

        return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $ssl_bytes);
    }
}
