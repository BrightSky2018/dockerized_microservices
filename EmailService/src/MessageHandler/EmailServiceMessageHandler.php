<?php

namespace App\MessageHandler;

use App\Message\EmailService;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class EmailServiceMessageHandler implements MessageHandlerInterface
{
    public function __invoke(EmailService $message)
    {
    }
}